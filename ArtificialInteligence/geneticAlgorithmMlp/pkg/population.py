import random
import sys
sys.path.append('pkg')
from individual import Individual
class Population:
    """Uma população é representado por um conjunto de individuos"""
    def __init__(self,size = 0,population = []):
        if len(population)> 0:
            self.population = population
            self.size = len(population)
        elif size > 0:
            self.population = []
            self.size = size
            label,weight,minWeight,csvPath
            label = [
             "energy",
             "statistics_energy",
             "kurtosis",
             "maximum",
             "minimum",
             "mean",
             "mean_deviation",             
             "img_range",
             "rms",
             "skewness",
             "std",
             "variance",
             "entropy",
             "uniformity"]
            weight = [1,1,1,1,1,1,1,1,1,1,1,1,1,1]
            minWeight = 0
            csvPath  =r'D:\01_Pessoal\Mestrado\Codes\visaocomputacional\ArtificialInteligence\geneticAlgorithmMlp\input-data\walvelet-features-tomato-sick-healthy.csv'           
            
            for i in range(0,self.size):
                self.population.append(Individual(label,weight,minWeight,csvPath))
    def getFittest(self):
        fitness = 0
        index = 0
        for i in range(0,self.size):
            indFitness = self.population[i].getFitness()
            if indFitness > fitness:
                fitness = self.population[i].getFitness()
                index = i

        return self.population[index]

    def getFittingSum(self):
        fittingSum = 0
        for ind in self.population:
            fittingSum += ind.getFitting()
        return fittingSum
    def getSecondFittest(self):
        fitness = 0
        fitness2 = 0

        for i in range(0,self.size):
            if self.population[i].getFitness() > self.population[fitness].getFitness():
                fitness2 = fitness
                fitness = i
            elif self.population[i].getFitness() > self.population[fitness2].getFitness():
                fitness2 = i
        return self.population[fitness2]
    def infactible(self):
        for ind in self.population:
            if ind.infactible():
                return True
        return  False
    def repair(self):
        """Repara indiviuos infactiveis alterando um gene aleatório"""
        for ind in self.population:
            if (ind.infactible()):
                # print("\nINDIVIDUO ANTES DA REPARAÇÃO")
                # print(ind)
                while (ind.infactible()):
                    repairPoint = random.randint(0, ind.size - 1)
                    if ind.chromosome[repairPoint] == 0:
                        ind.chromosome[repairPoint] = 1

                # print("\nINDIVIDUO DEPOIS DA REPARAÇÃO")
                # print(ind)
    def __str__(self):
        string =""
        for ind in self.population:
            string = string + str(ind) + "\n"
        return string
