class View:
    """Desenha o ambiente (o que está representado no Model) em formato texto."""
    def __init__(self, model):
        self.model = model

    def drawRowDivision(self):
        print("    ", end='')
        for _ in range(self.model.maze.maxColumns):
            print("+---", end='')
        print("+")

    def drawHeader(self):
        print("--- Estado do Ambiente ---")
        print("Posição agente  : {0},{1}".format(self.model.agentPos[0],self.model.agentPos[1]))
        objIndex = 1
        for goal in self.model.goalPos:
            print("Posição objetivo {0}: {1},{2}".format(objIndex,goal[0],goal[1]))
            objIndex += 1
        print("\n")
        # Imprime números das colunas
        print("   ", end='')
        for col in range(self.model.maze.maxColumns):
            print(" {0:2d} ".format(col), end='')

        print()
    def existGoal(self,row,col):
        existGoal = False
        for goal in self.model.goalPos:
            if goal[0] == row and goal[1] == col:
                existGoal = True
                break
        return existGoal
    def existBox(self,row,col):
        existBox = False
        for box in self.model.boxPos:
            if box[0] == row and box[1] == col:
                existBox = True
                break
        return existBox
    def draw(self):
        """Desenha o labirinto representado no modelo model."""
        self.drawHeader()

        for row in range(self.model.maze.maxRows):
            self.drawRowDivision()
            print(" {0:2d} ".format(row), end='') # Imprime número da linha

            for col in range(self.model.maze.maxColumns):
                if self.model.maze.walls[row][col] == 1: 
                    print("|XXX",end='')    # Desenha parede
                elif self.model.agentPos[0] == row and self.model.agentPos[1] == col:
                    print("| A ",end='')    # Desenha agente
                elif self.existBox(row,col):
                    print("| O ", end='')  # Desenha objeto (caixa)
                elif self.existGoal(row,col):
                    print("| G ",end='')    # Desenha objetivo
                else:
                    print("|   ",end='')    # Desenha vazio

            print("|")
            
            if row == (self.model.maze.maxRows - 1):
                self.drawRowDivision()