from view import View
from maze import Maze
from cardinal import *

class Model:
    """Model implementa um ambiente na forma de um labirinto com paredes e com um agente.
     A indexação da posição do agente é feita sempre por um par ordenado (lin, col). Ver classe Labirinto."""

    def __init__(self, rows, columns):
        """Construtor de modelo do ambiente físico (labirinto)
        @param rows: número de linhas do labirinto
        @param columns: número de colunas do labirinto
        """
        if rows <= 0:
            rows = 5
        if columns <= 0:
            columns = 5

        self.rows = rows
        self.columns = columns

        self.agentPos = [0,0]
        self.goalPos = []
        self.boxPos = []

        self.view = View(self)
        self.maze = Maze(rows,columns)

    def draw(self):
        """Desenha o labirinto em formato texto."""
        self.view.draw()

    def setAgentPos(self, row, col):
        """Utilizada para colocar o agente na posição inicial.
        @param row: a linha onde o agente será situado.
        @param col: a coluna onde o agente será situado.
        @return 1 se o posicionamento é possível, -1 se não for."""
        if (col < 0 or row < 0):
            return -1
        if (col >= self.maze.maxColumns or row >= self.maze.maxRows):
            return -1
        
        if self.maze.walls[row][col] == 1:
            return -1

        self.agentPos[0] = row
        self.agentPos[1] = col
        return 1
    
    def setGoalPos(self, goals):
        """Utilizada para colocar o objetivo na posição inicial.
        @param row: a linha onde o objetivo será situado.
        @param col: a coluna onde o objetivo será situado.
        @return 1 se o posicionamento é possível, -1 se não for."""
        for goal in goals:
            row = goal[0]
            col = goal[1]
            if (col < 0 or row < 0):
                return -1
            if (col >= self.maze.maxColumns or row >= self.maze.maxRows):
                return -1

            if self.maze.walls[row][col] == 1:
                return -1
            self.goalPos.append(goal)
            # self.goalPos[0] = row
            # self.goalPos[1] = col
        return 1

    def setBoxPos(self, boxes):
        """Utilizada para colocar a caixa na posição inicial.
        @param row: a linha onde a caixa será situado.
        @param col: a coluna onde a caixa será situado.
        @return 1 se o posicionamento é possível, -1 se não for."""
        for box in boxes:
            row = box[0]
            col = box[1]
            if (col < 0 or row < 0):
                return -1
            if (col >= self.maze.maxColumns or row >= self.maze.maxRows):
                return -1

            if self.maze.walls[row][col] == 1:
                return -1
            self.boxPos.append(box)
            # self.boxPos[0] = row
            # self.boxPos[1] = col
        return 1
    def go(self, direction):
        """Coloca o agente na posição solicitada pela ação go, desde que seja possível.
        Não pode ultrapassar os limites do labirinto nem estar em uma parede.
        @param direciton: inteiro de 0 a 7 representado as coordenadas conforme definido em cardinal.py"""
        rowAgent = self.agentPos[0]
        colAgent = self.agentPos[1]

        #Movimenta o agente
        if direction == N:
            rowAgent -= 1
        if direction == L:
            colAgent += 1
        if direction == S:
            rowAgent += 1
        if direction == O:
            colAgent -= 1
        #RESTRIÇÃO MOVIMENTOS APENAS NA VERTICAL
        # if direction == NE:
        #     row -= 1
        #     col += 1
        # if direction == NO:
        #     row -= 1
        #     col -= 1
        # if direction == SE:
        #     row += 1
        #     col += 1
        # if direction == SO:
        #     row += 1
        #     col -= 1

        # Verifica se está fora do grid
        if colAgent < 0 or colAgent >= self.maze.maxColumns:
            rowAgent = self.agentPos[0]
            colAgent = self.agentPos[1]
        if rowAgent < 0 or rowAgent >= self.maze.maxRows:
            rowAgent = self.agentPos[0]
            colAgent = self.agentPos[1]

        # Verifica se bateu na parede
        if self.maze.walls[rowAgent][colAgent] == 1:
            rowAgent = self.agentPos[0]
            colAgent = self.agentPos[1]

        # Verifica se bateu em alguma caixa
        for i in range(0,len(self.boxPos)):
            if self.boxPos[i][0] == rowAgent and self.boxPos[i][1] == colAgent:
                rowBox = self.boxPos[i][0]
                colBox = self.boxPos[i][1]
                #Movimenta a caixa virtualmente
                if direction == N:
                    rowBox -= 1
                if direction == L:
                    colBox += 1
                if direction == S:
                    rowBox += 1
                if direction == O:
                    colBox -= 1
                # Verifica se a nova posição da caixa está fora do grid
                if colBox < 0 or colBox >= self.maze.maxColumns:
                    # retrocede o movimento da caixa
                    rowBox = self.boxPos[i][0]
                    colBox = self.boxPos[i][1]
                    # retrocede o movimento do agente
                    rowAgent = self.agentPos[0]
                    colAgent = self.agentPos[1]
                if rowBox < 0 or rowBox >= self.maze.maxRows:
                    # retrocede o movimento da caixa
                    rowBox = self.boxPos[i][0]
                    colBox = self.boxPos[i][1]
                    # retrocede o movimento do agente
                    rowAgent = self.agentPos[0]
                    colAgent = self.agentPos[1]

                # Verifica se a caixa bateu em algum obstáculo
                if self.maze.walls[rowBox][colBox] == 1:
                    # retrocede o movimento da caixa
                    rowBox = self.boxPos[i][0]
                    colBox = self.boxPos[i][1]
                    # retrocede o movimento do agente
                    rowAgent = self.agentPos[0]
                    colAgent = self.agentPos[1]
                # Verifica se uma caixa bateu em outra
                for j in range(0,len(self.boxPos)):
                    if i != j:
                        if rowBox == self.boxPos[j][0] and colBox ==self.boxPos[j][1]:
                            # retrocede o movimento da caixa
                            rowBox = self.boxPos[i][0]
                            colBox = self.boxPos[i][1]
                            break


                self.boxPos[i] = [rowBox,colBox]
                #self.setBoxPos(rowBox, colBox)
        self.setAgentPos(rowAgent, colAgent)
