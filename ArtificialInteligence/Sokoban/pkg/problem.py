from maze import Maze
from state import State
from cardinal import *
from agent import *

class Problem:
    """Representação de um problema a ser resolvido por um algoritmo de busca clássica.
    A formulação do problema - instância desta classe - reside na 'mente' do agente."""


    def __init__(self):
        self.initialState = State(0,0)
        #self.goalState = State(0,0)
        self.goalState = [] #array de states
        self.intialBoxState1 = State(0,0)
        self.intialBoxState2 = State(0,0)
        self.intialBoxState3 = State(0,0)
        self.intialBoxState4 = State(0,0)
        self.intialBoxState5 = State(0,0)
        self.intialBoxState6 = State(0,0)
        self.intialStates = [self.initialState,self.intialBoxState1,self.intialBoxState2]
        #self.intialStates = [self.initialState,self.intialBoxState1,self.intialBoxState2,self.intialBoxState3,self.intialBoxState4,self.intialBoxState5,self.intialBoxState6]


    def createMaze(self, maxRows, maxColumns):
        """Este método instancia um labirinto - representa o que o agente crê ser o labirinto.
        As paredes devem ser colocadas fora desta classe porque este.
        @param maxRows: máximo de linhas do labirinto.
        @param maxColumns: máximo de colunas do labirinto."""
        self.mazeBelief = Maze(maxRows, maxColumns)
        self.maxRows = maxRows
        self.maxColumns = maxColumns
        self.cost = [[0.0 for j in range(maxRows*maxColumns)]for i in range(8)]

    def defInitialState(self, row, col):
        """Define o estado inicial.
        @param row: linha do estado inicial.
        @param col: coluna do estado inicial."""
        self.initialState.row = row
        self.initialState.col = col

    def defGoalState(self, goals):
        """Define o estado objetivo.
        @param row: linha do estado objetivo.
        @param col: coluna do estado objetivo."""
        for goal in goals:
            row = goal[0]
            col = goal[1]
            goalState = State(row,col)
            # self.goalState.row = row
            # self.goalState.col = col
            self.goalState.append(goalState)
    def defIntialBoxState(self,intialBoxes):
        """Define o estado inicial caixa.
                @param row: linha do estado inicial.
                @param col: coluna do estado inicial."""
        for i in range(1,len(self.intialStates)):
            #if i>0:#0 é o agente
            self.intialStates[i] = intialBoxes[i-1]
        # self.intialBoxState = intialBoxes
        # self.intialBoxState.row = row
        # self.intialBoxState.col = col
    def suc(self, state, action):
        """Função sucessora: recebe um estado e calcula o estado sucessor ao executar uma ação.
        @param state: estado atual.
        @param action: ação a ser realizado a partir do estado state.
        @param push: se true pode empurrar a caixa, se false, considera a caixa igual a parede
        @return estado sucessor"""
        nextState = []
        boxesStates = []
        rowAgent = state[0].row
        colAgent = state[0].col


        # Incrementa a linha e coluna de acordo com a respectiva ação
        # rowIncrement e colIncrement estão definidos em cardinal.py
        rowAgent += rowIncrement[action]
        colAgent += colIncrement[action]

        # Verifica limites
        if rowAgent < 0:
            rowAgent = 0
        if colAgent < 0:
            colAgent = 0
        if rowAgent == self.mazeBelief.maxRows:
            rowAgent = state[0].row
        if colAgent == self.mazeBelief.maxColumns:
            colAgent = state[0].col
        
        # Se tiver parede agente fica na posição original
        if self.mazeBelief.walls[rowAgent][colAgent] == 1:
            rowAgent = state[0].row
            colAgent = state[0].col

        # Teste de caixas
        for i in range(1,len(state)):
        #if i > 0: #0 é o agente
            rowBox = state[i].row
            colBox = state[i].col
            # Se tiver caixa empurra a caixa se possível
            if rowBox == rowAgent and colBox == colAgent:
                # incrementa posição da caixa
                rowBox += rowIncrement[action]
                colBox += colIncrement[action]
                #Verifica limites
                if rowBox < 0:
                    rowBox = state[i].row
                    rowAgent = state[0].row
                if colBox < 0:
                    colBox = state[i].col
                    colAgent = state[0].col
                if rowBox == self.mazeBelief.maxRows:
                    rowBox = state[i].row
                    rowAgent = state[0].row
                if colBox == self.mazeBelief.maxColumns:
                    colBox = state[i].col
                    colAgent = state[0].col
                # Verifica se a caixa bateu na parede
                if self.mazeBelief.walls[rowBox][colBox] == 1:
                    rowBox = state[i].row
                    colBox = state[i].col
                    rowAgent = state[0].row
                    colAgent = state[0].col
                #     #Verificar se bateu em outra caixa
                for j in range(1, len(state)):
                    if i!=j :#0 é o agente
                        rowOtherBox = state[j].row
                        colOtherBox = state[j].col
                        if rowBox == rowOtherBox and colBox == colOtherBox:
                            rowBox = state[i].row
                            colBox = state[i].col
                            rowAgent = state[0].row
                            colAgent = state[0].col

            boxesStates.append(State(rowBox,colBox))
        nextState.append(State(rowAgent, colAgent))
        for boxState in boxesStates:
            nextState.append(boxState)
        return nextState
        # return [State(rowAgent, colAgent),State(rowBox,colBox)]

    def possibleActions(self, state):
        """Retorna as ações possíveis de serem executadas em um estado.
        O valor retornado é um vetor de inteiros.
        Se o valor da posição é -1 então a ação correspondente não pode ser executada, caso contrário valerá 1.
        Exemplo: se retornar [-1, -1, -1, 1, 1, -1, -1, -1] apenas as ações 3 e 4 podem ser executadas, ou seja, apenas SE e S.
        @param state: estado atual.
        @return ações possíveis"""
        
        actions = [1,1,1,1,1,1,1,1] # Supõe que todas as ações são possíveis
        

        
        row = state.row
        col = state.col 

        # Esta no limite superior, não pode ir para N, NE ou NO
        if row == 0: 
            actions[N] = actions[NE] = actions[NO] = -1
        # Esta no limite direito, não pode ir para NE, L ou SE
        if col == self.mazeBelief.maxColumns - 1:
            actions[NE] = actions[L] = actions[SE] = -1
        # Esta no limite inferior, não pode ir para SE, S ou SO
        if row == self.mazeBelief.maxRows - 1:
            actions[SE] = actions[S] = actions[SO] = -1
        # Esta no limite esquerdo, não pode ir para SO, O ou NO
        if col == 0:
            actions[SO] = actions[O] = actions[NO] = -1

        walls = self.mazeBelief.walls
        # Testa se há parede nas direções
        if actions[N] != -1 and walls[row - 1][col] == 1: # Norte
            actions[N] = -1
        if actions[L] != -1 and walls[row][col + 1] == 1: # Leste
            actions[L] = -1
        if actions[S] != -1 and walls[row + 1][col] == 1: # Sul
            actions[S] = -1
        if actions[O] != -1 and walls[row ][col - 1] == 1: # Oeste
            actions[O] = -1

        if actions[NE] != -1 and walls[row - 1][col + 1] == 1: # Nordeste
            actions[NE] = -1
        if actions[NO] != -1 and walls[row - 1][col - 1] == 1: # Noroeste
            actions[NO] = -1
        if actions[SE] != -1 and walls[row + 1][col + 1] == 1: # Sudeste
            actions[SE] = -1
        if actions[SO] != -1 and walls[row + 1][col - 1] == 1: # Sudoeste
            actions[SO] = -1

        return actions

    def possibleActionsWithoutCollaterals(self, state):
        """Retorna as ações possíveis de serem executadas em um estado, desconsiderando movimentos na diagonal.
        O valor retornado é um vetor de inteiros.
        Se o valor da posição é -1 então a ação correspondente não pode ser executada, caso contrário valerá 1.
        Exemplo: se retornar [1, -1, -1, -1, -1, -1, -1, -1] apenas a ação 0 pode ser executada, ou seja, apena N.
        @param state: estado atual.
        @param agent: se verdadeiro é o agente, se falso é a caixa
         @param push: se true pode empurrar a caixa, se false, considera a caixa igual a parede
        @return ações possíveis"""
        
        actions = [1,-1,1,-1,1,-1,1,-1] # Supõe que todas as ações (menos na diagonal) são possíveis


        rowAgent = state[0].row
        colAgent = state[0].col

        # rowBox = state[1].row
        # colBox = state[1].col

        # Esta no limite superior, não pode ir para N
        if rowAgent == 0:
            actions[N] = -1
        # Esta no limite direito, não pode ir para L
        if colAgent == self.mazeBelief.maxColumns - 1:
            actions[L] = -1
        # Esta no limite inferior, não pode ir para S
        if rowAgent == self.mazeBelief.maxRows - 1:
            actions[S]= -1
        # Esta no limite esquerdo, não pode ir para O
        if colAgent == 0:
            actions[O] = -1

        walls = self.mazeBelief.walls
        # Testa se há parede nas direções
        if actions[N] != -1 and walls[rowAgent - 1][colAgent] == 1:  # Norte
            actions[N] = -1
        if actions[L] != -1 and walls[rowAgent][colAgent + 1] == 1:  # Leste
            actions[L] = -1
        if actions[S] != -1 and walls[rowAgent + 1][colAgent] == 1:  # Sul
            actions[S] = -1
        if actions[O] != -1 and walls[rowAgent][colAgent - 1] == 1:  # Oeste
            actions[O] = -1
        #Teste de caixas
        for i in range(1,len(state)):
            #0 é o agente
            rowBox = state[i].row
            colBox = state[i].col
            #testa se empurra alguma caixa fora do limite ou contra parede
            if actions[N] != -1 and rowBox == rowAgent - 1 and colBox == colAgent:  # Norte
                if rowBox == 0:
                    actions[N] = -1
                if walls[rowBox-1][colBox] == 1:
                    actions[N] = -1
            if actions[L] != -1 and rowBox == rowAgent and colBox == colAgent + 1:  # Leste
                if colBox == self.mazeBelief.maxColumns - 1:
                    actions[L] = -1
                if walls[rowBox][colBox+1] == 1:
                    actions[L] = -1
            if actions[S] != -1 and rowBox == rowAgent + 1 and colBox == colAgent:  # Sul
                if rowBox == self.mazeBelief.maxRows - 1:
                    actions[S] = -1
                if walls[rowBox + 1][colBox] == 1:
                    actions[S] = -1
            if actions[O] != -1 and rowBox == rowAgent and colBox == colAgent - 1:  # Oeste
                if colBox == 0:
                    actions[O] = -1
                if walls[rowBox][colBox - 1] == 1:
                    actions[O] = -1

            #testar se empurra uma caixa contra outra
            for j in range(1,len(state)):
                if i != j :# se não é a mesma caixa
                    rowOtherBox = state[j].row
                    colOtherBox = state[j].col
                    if actions[N] != -1 and rowOtherBox == rowBox - 1 and colOtherBox == colBox:  # Norte
                        if rowBox == rowAgent-1 and colBox == colAgent:
                            actions[N] = -1
                    if actions[L] != -1 and rowOtherBox == rowBox and colOtherBox == colBox + 1:  # Leste
                        if rowBox == rowAgent and colBox == colAgent + 1:
                            actions[L] = -1
                    if actions[S] != -1 and rowOtherBox == rowBox + 1 and colOtherBox == colBox:  # Sul
                        if rowBox == rowAgent + 1 and colBox == colAgent:
                            actions[S] = -1
                    if actions[O] != -1 and rowOtherBox == rowBox and colOtherBox == colBox - 1:  # Oeste
                        if rowBox == rowAgent and colBox == colAgent - 1:
                            actions[O] = -1

        return actions

    def getActionCost(self, action):
        """Retorna o custo da ação.
        @param action:
        @return custo da ação"""
        if (action == N or action == L or action == O or action == S):
            return 1.0
        else:
            return 1.5

    def goalTest(self, currentState):
        """Testa se alcançou o estado objetivo.
        @param currentState: estado atual.
        @return True se o estado atual for igual ao estado objetivo."""
        goalTest = False
        goalTests = 0
        for goal in self.goalState:
            for i in range(1,len(currentState)):
                #if i > 0: # 0 é o agente
                if currentState[i] == goal:
                    goalTests += 1
        #REMOVER -1 OU NAO
        if goalTests == len(self.goalState):
            goalTest = True
        # return currentState == self.goalState # Operador de igualdade definido em __eq__ no arquivo state.py
        return goalTest
