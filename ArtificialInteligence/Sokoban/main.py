import sys
sys.path.append('pkg')
from model import Model
from agent import Agent

def buildMaze(model):
    model.maze.putVerticalWall(0,7,0)
    model.maze.putVerticalWall(3,5,4)
    model.maze.putVerticalWall(0,7,7)
    # model.maze.putVerticalWall(5,5,2)
    # model.maze.putVerticalWall(8,8,2)

    model.maze.putHorizontalWall(1,6,0)
    model.maze.putHorizontalWall(1,6,7)
    model.maze.putHorizontalWall(1,2,3)
    # model.maze.putHorizontalWall(3,5,3)
    # model.maze.putHorizontalWall(7,7,3)

    # model.maze.putVerticalWall(6,7,4)
    # model.maze.putVerticalWall(5,6,5)
    # model.maze.putVerticalWall(5,7,7)

def main():
    # Cria o ambiente (modelo) = Labirinto com suas paredes
    mazeRows = 8
    mazeColumns = 8
    model = Model(mazeRows, mazeColumns)
    buildMaze(model)

    # Define a posição inicial do agente no ambiente - corresponde ao estado inicial
    model.setAgentPos(4,1)

    # Define a posição inicial  da caixa no ambiente
    box1 = [5, 2]
    box2 = [5, 3]
    # box3 = [3, 3]
    # box4 = [2, 2]
    # box5 = [2, 4]
    # box6 = [4, 5]
    boxes = [box1,box2]
    #boxes = [box1,box2,box3,box4,box5,box6]
    model.setBoxPos(boxes)

    # Cria um agente
    agent = Agent(model)

    model.draw()
    print("\n Início do ciclo de raciocínio do agente \n")
    while agent.deliberate() != -1:
        model.draw()


if __name__ == '__main__':
    main()
