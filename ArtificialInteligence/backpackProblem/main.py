import random #Utilizada para gerar numeros de forma aleátoria
import copy #Utilizada para copiar objetos
import matplotlib.pyplot as plt #Utilizada pra plotar gráficos

import sys #Utilizada para importar classes da pasta pkg
sys.path.append('pkg')
from population import Population #Classe população

def main():
#======================Funções auxiliares====================#
    def rouletteWheel():
        """Cria nova população baseado no método da roleta """
        fittingSum = pop.getFittingSum()
        fitnessProbability = []
        chooseProbabilities = []
        populationArray = []
        popRoullete = copy.deepcopy(pop)

        #Calcula fitnessProbability para cada individuo
        for ind in popRoullete.population:
            if fittingSum == 0:
                fitnessProbability.append(1/popRoullete.size)
            else:
                fitnessProbability.append(ind.getFitting()/fittingSum)

        #Calcula probabilidades randomicas para cada individuo a ser utulizada no metodo da roleta
        for i in range (0,popRoullete.size):
            chooseProbabilities.append(random.random())
        chooseProbabilities.sort()

        #Seleciona população para cruzamento baseado no método da roleta
        for i in range (0,len(chooseProbabilities)):
           sumFitnessProbability = 0
           for j in range (0,len(fitnessProbability)):
                sumFitnessProbability += fitnessProbability[j]
                if chooseProbabilities[i] < sumFitnessProbability:
                    populationArray.append(popRoullete.population[j])
                    break
        return Population(0,populationArray)


    def crossover(Pc):
        """Cruza individuos selecionados pelo método da roleta baseado na probabilidade pc e no ponto de crossOver obtido randomicamente"""
        offspringArray = []
        offspringCrossOver = copy.deepcopy(popSelected)
        for i in range (0,len(offspringCrossOver.population)-1,2):
            if random.random() < Pc:
                crossOverPoint = random.randint(0,offspringCrossOver.population[0].size-1)
                individual1 = offspringCrossOver.population[i]
                individual2 = offspringCrossOver.population[i+1]
                for j in range(crossOverPoint,offspringCrossOver.population[0].size):
                    temp = individual1.chromosome[j]
                    individual1.chromosome[j] = individual2.chromosome[j]
                    individual2.chromosome[j] = temp
                offspringArray.append(individual1)
                offspringArray.append(individual2)
        return Population(0,offspringArray)


    def mutation(Pm):
        """Seleciona os 2 melhores individuos para mutação"""
        for child in offspring.population:
            for gene in child.chromosome:
                if random.random() < Pm:
                    if child.chromosome[gene] == 0:
                        child.chromosome[gene] = 1
                    else:
                        child.chromosome[gene] = 0


#================================================================================#
    for i in range (0,10):
        """Início do codigo principal"""
        Pm = 0.6 #probabilidade de fazer mutação
        Pc = 0.8  #probabilidade de fazer crossover
        Np = 3000 #tamanho da população inicial
        # maxFitness = 10
        maxGeneration = 100
        repair = False # False penalização; True reparação;
        fitnessArray = []
        generationArray = []

        pop = Population(Np)
        # print("\nPopulação Inicial")
        # print(pop)
        # for individual in pop.population:
        #     print(individual)
        generation = 0
        generationArray.append(generation)

        print("\nIndividuo com melhor fitness geração {0:d}:".format(generation))
        print(pop.getFittest())

        fitnessArray.append((pop.getFittest().getFitness()))
        if repair:
           if(pop.infactible()):
               pop.repair()



        # while pop.getFittest().getFitness()< maxFitness and generation < maxGeneration:
        while generation < maxGeneration:

            #Seleciona individuos para reprodução
            popSelected = rouletteWheel()

            # print ("\nPopulação selecionada para reprodução por rouletteWheel\n")
            # print(popSelected)

            # Faz crossover com os individuos selecionados sobre probabilidade randomica
            offspring = crossover(Pc)

            if hasattr(offspring,'population'):
                # print ("\nNova População descendente obtida por crossOver\n")
                # print(offspring)

                if repair:
                    if (offspring.infactible()):
                        offspring.repair()

                # Faz mutation em cada gene de cada individuo offspring sobre probabilidade randomica
                mutation(Pm)
                # print ("\nNova População descendente obtida pós mutação\n")
                # print(offspring)
                if repair:
                    if (offspring.infactible()):
                        offspring.repair()


                # cria nova população unindo população e filhos
                currentPopulationArray = copy.deepcopy(pop.population)

                offspringArray = copy.deepcopy(offspring.population)
                for child in offspringArray:
                    currentPopulationArray.append(child)
                currentPopulation = Population(0,currentPopulationArray)

                # Ordena pelo fitness
                currentPopulation.population.sort(key=lambda x: x.getFitness(), reverse = True)  # Ordena a geração pelo fitness, descendente

                # print("\n Ordenado")
                # print(currentPopulation)

                #Cria nova população com os melhores fitness obtidos
                newGenarationArray = []
                for i in range (0,Np-1):
                    newGenarationArray.append(currentPopulation.population[i])
                pop = Population(0,newGenarationArray)
                # print ("\nNova populção")
                # print(pop)

                # print("\nNova População selecionada")
                # for ind in pop.population:
                #     print(ind)
            generation +=1
            generationArray.append(generation)
            # print("\nIndividuo com melhor fitness geração {0:d}:".format(generation))
            fitnessArray.append((pop.getFittest().getFitness()))
            # print(pop.getFittest())

        print("********** Melhor individuo obtido **********")
        print(pop.getFittest())
        print(fitnessArray)
        # if(repair):
        #     plt.title("Repair")
        # else:
        #     plt.title("Penalty")
        #
        # plt.plot(generationArray,fitnessArray)
        # plt.axis([0, maxGeneration, 0, 300])
        # plt.ylabel("Fitness")
        # plt.xlabel("Generation")
        # plt.show()
if __name__ == '__main__':
    main()