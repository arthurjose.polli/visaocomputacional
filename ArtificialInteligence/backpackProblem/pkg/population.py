import random
import sys
sys.path.append('pkg')
from individual import Individual
class Population:
    """Uma população é representado por um conjunto de individuos"""
    def __init__(self,size = 0,population = []):
        if len(population)> 0:
            self.population = population
            self.size = len(population)
        elif size > 0:
            self.population = []
            self.size = size
            value = [1,3,1,8,9,3,2,8,5,1,1,6,3,2,5,2,3,8,9,3,2,4,5,4,3,1,3,2,14,32,20,19,15,37,18,13,19,10,15,40,17,39]
            weight = [3,8,12,2,8,4,4,5,1,1,8,6,4,3,3,5,7,3,5,7,4,3,7,2,3,5,4,3,7,19,20,21,11,24,13,17,18,6,15,25,12,19]
            maxWeight = 120
            for i in range(0,self.size):
                self.population.append(Individual(value,weight,maxWeight))
    def getFittest(self):
        fitness = 0
        index = 0
        for i in range(0,self.size):
            indFitness = self.population[i].getFitness()
            if indFitness > fitness:
                fitness = self.population[i].getFitness()
                index = i

        return self.population[index]

    def getFittingSum(self):
        fittingSum = 0
        for ind in self.population:
            fittingSum += ind.getFitting()
        return fittingSum
    def getSecondFittest(self):
        fitness = 0
        fitness2 = 0

        for i in range(0,self.size):
            if self.population[i].getFitness() > self.population[fitness].getFitness():
                fitness2 = fitness
                fitness = i
            elif self.population[i].getFitness() > self.population[fitness2].getFitness():
                fitness2 = i
        return self.population[fitness2]
    def infactible(self):
        for ind in self.population:
            if ind.infactible():
                return True
        return  False
    def repair(self):
        """Repara indiviuos infactiveis alterando um gene aleatório"""
        for ind in self.population:
            if (ind.infactible()):
                # print("\nINDIVIDUO ANTES DA REPARAÇÃO")
                # print(ind)
                while (ind.infactible()):
                    repairPoint = random.randint(0, ind.size - 1)
                    if ind.chromosome[repairPoint] == 1:
                        ind.chromosome[repairPoint] = 0

                # print("\nINDIVIDUO DEPOIS DA REPARAÇÃO")
                # print(ind)
    def __str__(self):
        string =""
        for ind in self.population:
            string = string + str(ind) + "\n"
        return string
