import sys
import timeit
import numpy as np
import cv2
import math
def meanFilterIngenuous(img,h,w):
    rows, cols, channels = img.shape
    img_channels = cv2.split(img)
    img_channels_filter = [np.zeros((rows,cols)),np.zeros((rows,cols)),np.zeros((rows,cols))]

    index = -1
    for band in img_channels:
        index += 1
        for row in range(rows):
            for col in range(cols):
                soma = 0
                den = 0
                for j in range(math.trunc(-h/2),math.trunc(h/2)+1):
                    for i in range(math.trunc(-w/2),math.trunc(w/2)+1):
                        windowRow = row+j
                        windowCol = col+i
                        if not (windowRow < 0 or windowRow > rows-1 or windowCol < 0 or windowCol > cols-1):
                            den += 1
                            soma += band[windowRow,windowCol]
                img_channels_filter[index][row,col] = soma/den

    img2 = cv2.merge(img_channels_filter)
    return img2


def meanFilterSeparable(img,h,w):
    start_time = timeit.default_timer()
    rows, cols, channels = img.shape
    img_channels = cv2.split(img)
    img_channels_horinzontal_filter = [np.zeros((rows, cols)), np.zeros((rows, cols)), np.zeros((rows, cols))]
    img_channels_filter = [np.zeros((rows, cols)), np.zeros((rows, cols)), np.zeros((rows, cols))]
    index = -1
    for band in img_channels:
        index += 1
        for row in range(rows):
            for col in range(cols):
                soma = 0
                den = 0
                for i in range(math.trunc(-w / 2), math.trunc(w / 2) + 1):
                        windowCol = col + i
                        if not (windowCol < 0 or windowCol > cols - 1):
                            den += 1
                            soma += band[row, windowCol]
                img_channels_horinzontal_filter[index][row, col] = soma / den
    index = -1
    for band in img_channels_horinzontal_filter:
        index += 1
        for row in range(rows):
            for col in range(cols):
                soma = 0
                den = 0
                for j in range(math.trunc(-h / 2), math.trunc(h / 2) + 1):
                    windowRow = row + j
                    if not (windowRow < 0 or windowRow > rows - 1):
                        den += 1
                        soma += band[windowRow, col]
                img_channels_filter[index][row, col] = soma / den
    img2 = cv2.merge(img_channels_filter)

    return img2

def meanFilterIntegral(img,integral,h,w):

    rows, cols, channels = img.shape
    img_channels = cv2.split(img)
    integral_channels = cv2.split(integral)
    img_channels_filter = [np.zeros((rows, cols)), np.zeros((rows, cols)), np.zeros((rows, cols))]
    half_h = math.trunc(h / 2)
    half_w = math.trunc(w / 2)
    index = -1
    for band in img_channels:
        index += 1
        for row in range(rows):
            for col in range(cols):
                if (row - half_h < 0 or row + half_h > rows-1 or col - half_w < 0 or col + half_w > cols-1):
                    half_h_resize = half_h -1
                    half_w_resize = half_w -1
                    while(row - half_h_resize < 0 or row + half_h_resize > rows-1 or col - half_w_resize < 0 or col + half_w_resize > cols-1):
                        half_h_resize -= 1
                        half_w_resize -= 1
                    if(not half_h_resize == 0):
                        img_channels_filter[index][row, col] =  (integral_channels[index][row+half_h_resize,col+half_w_resize]+\
                                                              integral_channels[index][row-half_h_resize,col-half_w_resize]-\
                                                              integral_channels[index][row-half_h_resize,col+half_w_resize]-\
                                                              integral_channels[index][row+half_h_resize,col-half_w_resize])/(half_h_resize*2+1)*(half_w_resize*2+1)

                    else:
                        img_channels_filter[index][row, col] = band[row, col]

                else:
                    img_channels_filter[index][row,col] = (integral_channels[index][row+half_h,col+half_w]+\
                                                          integral_channels[index][row-half_h,col-half_w]-\
                                                          integral_channels[index][row-half_h,col+half_w]-\
                                                          integral_channels[index][row+half_h,col-half_w])/h*w
    img1 = cv2.merge(img_channels_filter)
    cv2.imshow("teste",img1)
    cv2.waitKey()
    cv2.destroyAllWindows()
    for band in img_channels_filter:
        smallest = np.amin(band)
        biggest = np.amax(band)
        for row in range(rows):
            for col in range(cols):
                #tentativa de extrair as bordas pretas sem sucesso
                #if not (row - half_h < 0 or row + half_h > rows-1 or col - half_w < 0 or col + half_w > cols-1):
                band[row,col] = (band[row,col]-smallest)/(biggest-smallest)


    img2 = cv2.merge(img_channels_filter)


    return img2


def main():
    """Parâmetros iniciais"""
    h = 9 #Altura da janela
    w = 9 #Largura da janela
    input_image = r'C:\MESTRADO\Codes\visaocomputacional\DigitalImageProessing\Blur\input-images\00-original.bmp'
    original_img = cv2.imread (input_image)

    if original_img is None:
        print ('Erro abrindo a imagem.\n')
        sys.exit ()

    # Converte para float32
    original_img = original_img.astype (np.float32) / 255

    # Mostra imagem
    # cv2.imshow ('00-original', original_img[:])
    # cv2.waitKey ()
    # cv2.destroyAllWindows ()

    #Aplica filtro da média ingenuo
    """start_time = timeit.default_timer()
    imgFilter1 = meanFilterIngenuous(original_img,h,w)
    print("Tempo algoritmo ingenuo  para janela {0:d}x{1:d} = {2:f}".format(h, w, timeit.default_timer() - start_time))
    saida2 = r'C:\MESTRADO\Codes\visaocomputacional\DigitalImageProessing\Blur\output-images\01-IngenuosMeanFilter.png'
    cv2.imwrite(saida2, imgFilter1 * 255)"""
    # cv2.imshow('01-MeanFilter', imgFilter1)
    # cv2.waitKey()
    # cv2.destroyAllWindows()

    #Aplica filtro da média separavel
    """start_time = timeit.default_timer()
    imgFilter2 = meanFilterSeparable(original_img,h,w)
    print(
        "Tempo algoritmo separavel  para janela {0:d}x{1:d} = {2:f}".format(h, w, timeit.default_timer() - start_time))
    saida2 = r'C:\MESTRADO\Codes\visaocomputacional\DigitalImageProcessing\Blur\output-images\02-SeparableMeanFilter.png'
    cv2.imwrite(saida2, imgFilter2 * 255)"""
    # cv2.imshow('02-SeparableMeanFilter', imgFilter2)
    # cv2.waitKey()
    # cv2.destroyAllWindows()


    #Aplica o filtro da média usando imagem integral
    #Cria imagem integral
    integral_img_channels = []
    img_channels = cv2.split(original_img)
    for band in img_channels:
        integral_img_channels.append(cv2.integral(band))
    integral_img = cv2.merge(integral_img_channels)

    #Aplica filtro com da média utilizando imagem integral
    start_time = timeit.default_timer()
    imgFilter3 = meanFilterIntegral(original_img,integral_img,h,w)
    print("Tempo algoritmo integral  para janela {0:d}x{1:d} = {2:f}".format(h, w, timeit.default_timer() - start_time))
    cv2.imshow('03-IntegralMeanFilter', imgFilter3)
    cv2.waitKey()
    cv2.destroyAllWindows()
    saida2 = r'C:\MESTRADO\Codes\visaocomputacional\DigitalImageProcessing\Blur\output-images\03-IntegralMeanFilter.png'
    cv2.imwrite(saida2, imgFilter3 * 255)

if __name__ == '__main__':
    main()
