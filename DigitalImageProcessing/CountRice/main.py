import sys
import timeit
import cv2
import numpy as np
import glob
import statistics
import math

def createconnectedComponents(img):
    # Limiarização adaptativa utilizando uma distribuição gaussiana

    img = cv2.adaptiveThreshold(img, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 17, 0)

    # ABERTURA
    kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (5, 5))
    img = cv2.morphologyEx(img, cv2.MORPH_OPEN, kernel, iterations=1)
    kernel = cv2.getStructuringElement(cv2.MORPH_CROSS, (3, 3))
    img = cv2.morphologyEx(img, cv2.MORPH_OPEN, kernel, iterations=3)


    # Rotula imagem
    retval, labels, stats, centroids = cv2.connectedComponentsWithStats(img)
    # Remove background, primeiro item, Não é a solução mais genérica, mas resolve para o grupo de imagens propostas!
    stats = stats[1:, :]

    return stats

def splitConnectedComponentsStatistically(stats):

    # areas = stats[:,-1]
    # mean_area = statistics.mean(areas)
    # std_area = statistics.stdev(areas)
    # print("Area")
    # print("Mean: {0:f}, Std: {1:f}".format(mean_area,std_area))
    # print(areas)
    # print("\n")

    widths = stats[:,2]
    mean_widths = statistics.mean(widths)
    std_widths = statistics.stdev(widths)
    # print("Width \n")
    # print ("Mean: {0:f}, Std: {1:f}".format(mean_widths, std_widths))
    # print(widths)
    # print("\n")

    heights = stats[:, 3]
    mean_heights = statistics.mean(heights)
    std_heights = statistics.stdev(heights)
    # print("Height \n")
    # print ("Mean: {0:f}, Std: {1:f}".format(mean_heights, std_heights))
    # print(heights)
    # print("\n")
    components = []
    for blob in stats:
        width = blob[2]
        height = blob[3]
        # area =  blob[4]
        # num_blobs = 1
        num_horizontal_blobs = 1
        num_vertical_blobs = 1
        if (width < mean_widths - std_widths * 2):
            num_horizontal_blobs = 0
        else:
            while(width/num_horizontal_blobs)>(mean_widths+std_widths*2):
                num_horizontal_blobs += 1
        if(height < mean_heights - std_heights*2):
            num_vertical_blobs = 0
        else:
            while (height / num_vertical_blobs) > (mean_heights + std_heights*2):
                num_vertical_blobs += 1

        if not(num_horizontal_blobs == 0 or num_vertical_blobs == 0):
            pt1 = (blob[0],blob[1])
            pt2 = (blob[0]+width, blob[1]+height)
            color = (0,255,0)
            # thickness é igual a quantidade de arroz que existe em um blob
            thickness = num_horizontal_blobs*num_vertical_blobs
            components.append([pt1,pt2,color,thickness])
    return components

def main():
    path = r'C:\MESTRADO\Codes\visaocomputacional\DigitalImageProcessing\CountRice\input-images\*.*'
    for file in glob.glob(path):
        #Lendo imagem com 8 bits
        img = cv2.imread(file,cv2.IMREAD_ANYDEPTH)
        original_img = cv2.imread(file)
        if img is None:
            print ('Erro abrindo a imagem.\n')
            sys.exit()

        stats = createconnectedComponents(img)
        components = splitConnectedComponentsStatistically(stats)
        rices = 0
        for component in components:
            cv2.rectangle(original_img,component[0],component[1],component[2],component[3])
            rices += component[3]
        print("Image: {0:s}".format(file))
        print("Number of rices: {0:d}".format(rices))
        # Mostra imagem
        saida = file.replace("input-images","output-images")
        cv2.imwrite(saida, original_img )
        # cv2.imshow("Detectados", original_img)
        # cv2.waitKey()
        # cv2.destroyAllWindows()

if __name__ == '__main__':
    main()